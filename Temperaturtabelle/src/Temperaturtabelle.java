
public class Temperaturtabelle {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s %n", "Fahrenheit", "Celsius");
		System.out.printf("------------------------ %n");
		double firstValue = -28.8889;
		double firstValueRounded = Math.round(firstValue*100.0)/100.0;
		System.out.printf("%-12s|%10.6s %n", "-20", firstValueRounded);
		double secondValue = -23.3333;
		double secondValueRounded = Math.round(secondValue*100.0)/100.0;
		System.out.printf("%-12s|%10.6s %n", "-10", secondValueRounded);
		double thirdValue = -17.7778;
		double thirdValueRounded = Math.round(thirdValue*100.0)/100.0;
		System.out.printf("%-12s|%10.6s %n", "+0", thirdValueRounded);
		double fourthValue = -6.6667;
		double fourthValueRounded = Math.round(fourthValue*100.0)/100.0;
		System.out.printf("%-12s|%10.6s %n", "+20", fourthValueRounded);
		double fifthValue = -1.1111;
		double fifthValueRounded = Math.round(fifthValue*100.0)/100.0;
		System.out.printf("%-12s|%10.6s %n", "+30", fifthValueRounded);
	}

}