import java.util.Scanner;

public class Fallunterscheidungen {

	public static void main(String[] args) {
		int noten;
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Bitte gebe eine Note ein:");
		noten = myScanner.nextInt();
		
		switch(noten) {
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4:
			System.out.println("Ausreichend");
			break;
		case 5:
			System.out.println("Mangelhaft");
			break;
		case 6:
			System.out.println("Ungenügend");
			break;
		default:
			System.out.println("Bitte nur Noten von 1 bis 6 angeben.");
			break;
		}
	}
}