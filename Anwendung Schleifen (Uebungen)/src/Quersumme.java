import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		//Aufgabe 3: Quersumme
        Scanner input = new Scanner(System.in);
        System.out.print("Geben Sie eine Zahl ein: \n");
        int zahlEingabe = input.nextInt();
        
        int summe = 0;
        
        do {
        	summe += zahlEingabe %10;
        	zahlEingabe /= 10;
        	
        } while (zahlEingabe > 0);
        System.out.println(summe);
	}

}
