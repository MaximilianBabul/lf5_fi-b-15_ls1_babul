import java.util.Scanner;

public class ZaehlenFor {
	//Aufgabe 1: Z�hlen
	public static void main(String[] args) {
		
        Scanner input = new Scanner(System.in);
        System.out.print("Geben Sie eine Zahl ein.");
        int zahlEingabe = input.nextInt();
        
        for(int zahl = 1; zahl < zahlEingabe+1; zahl++){
            System.out.printf("%d, ", zahl);
       }
        
        System.out.println();
        
        for(int zahl = zahlEingabe; zahl > 0; zahl--){
            System.out.printf("%d, ", zahl);
       }

	}
}
