public class Modulo {
	// Aufgabe 3: Modulo
	public static void main(String[] args) {
		System.out.print("Diese Zahlen sind durch 7 teilbar: \n");
		for (int zahl = 1; zahl <= 200; zahl++) {
			if (zahl % 7 == 0) {
				System.out.println(zahl);
			}
		}
		
        System.out.println();

		System.out.print("Diese Zahlen sind nicht durch 5, aber durch 4 teilbar: \n");
		for (int zahl = 1; zahl <= 200; zahl++) {
			if (zahl % 5 != 0 && zahl % 4 == 0) {
				System.out.println(zahl);

			}
		}
	}
}
