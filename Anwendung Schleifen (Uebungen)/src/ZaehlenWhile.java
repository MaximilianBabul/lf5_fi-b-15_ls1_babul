import java.util.Scanner;

public class ZaehlenWhile {
	//Aufgabe 1: Z�hlen
	public static void main(String[] args) {
		
        Scanner input = new Scanner(System.in);
        System.out.print("Geben Sie eine Zahl ein.");
        int zahlEingabe = input.nextInt();
        
        int zahl = 1;
        while (zahl < zahlEingabe+1) {
        	System.out.printf("%d, ", zahl);
        	zahl++;
        }
        
        System.out.println();
        
        zahl = zahlEingabe;
        while (zahl > 0) {
        	System.out.printf("%d, ", zahl);
        	zahl--;
        }
	}

}