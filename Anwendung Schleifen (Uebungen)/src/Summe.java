import java.util.Scanner;

public class Summe {
	//Aufgabe 2: Summe
	public static void main(String[] args) {
		
        Scanner input = new Scanner(System.in);
        System.out.print("Geben Sie bitte einen begrenzenden Wert ein:");
        int zahlEingabe = input.nextInt();
        
        int summe = 0;
        for(int zahl = 0; zahl < zahlEingabe+1; zahl++){
        	summe += zahl;
       }
        System.out.printf("Die Summe f�r A betr�gt:" + summe);
       
        System.out.println();
        
        summe = 0;
        for(int zahl = 0; zahl < zahlEingabe+1; zahl++){
        	summe += 2*zahl;
       }
        System.out.printf("Die Summe f�r B betr�gt:" + summe);
        
        System.out.println();

        summe = 0;
        for(int zahl = 0; zahl < zahlEingabe+1; zahl++){
        	summe += 2*zahl+1;
       }
        System.out.printf("Die Summe f�r C betr�gt:" + summe);
	}
}
